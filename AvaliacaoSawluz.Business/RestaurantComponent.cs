﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvaliacaoSawluz.Data.Models;
using AvaliacaoSawluz.Data.Context;
using System.Data.Entity.Migrations;
using AvaliacaoSawluz.Business.Exceptions;

namespace AvaliacaoSawluz.Business
{
    public class RestaurantComponent : IRestaurantComponent
    {
        /// <summary>
        /// Get an specific entry of Restaurant entity
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns></returns>
        public Restaurant GetRestaurant(long id)
        {
            RestaurantContext ctx = new RestaurantContext();
            return ctx.Restaurants.FirstOrDefault(f => f.Id == id);
        }

        /// <summary>
        /// Lists All Restaurant Entity Entries
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Restaurant> ListRestaurants()
        {
            RestaurantContext ctx = new RestaurantContext();
            return ctx.Restaurants.ToList();
        }

        /// <summary>
        /// Remover an specific entry of Restaurant Entity
        /// </summary>
        /// <param name="id"></param>
        public void RemoveRestaurant(long id)
        {
            RestaurantContext ctx = new RestaurantContext();
            ctx.Restaurants.Remove(ctx.Restaurants.First(f => f.Id == id));
            ctx.SaveChanges();
        }

        /// <summary>
        /// Creates or Update an Restaurant Entity
        /// </summary>
        /// <param name="restaurant"></param>
        public void SaveRestaurant(Restaurant restaurant)
        {
            RestaurantContext ctx = new RestaurantContext();
            //Check if Restaurant Already Exists

            Restaurant item = ctx.Restaurants.First(f => f.Id == restaurant.Id);
            if (ctx.Restaurants.Any(f => f.Name.ToLower() == restaurant.Name.ToLower() && restaurant.Id != f.Id))
            {
                throw new AlreadyExistsException(restaurant.Name);
            }

            ctx.Restaurants.AddOrUpdate(f => f.Id, restaurant);

            ctx.SaveChanges();

        }

    }
}
