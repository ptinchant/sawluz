﻿using AvaliacaoSawluz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoSawluz.Business
{
    public interface IRestaurantComponent
    {

        IEnumerable<Restaurant> ListRestaurants();
        Restaurant GetRestaurant(long id);
        void SaveRestaurant(Restaurant restaurant);
        void RemoveRestaurant(long id);
        
    }
}
