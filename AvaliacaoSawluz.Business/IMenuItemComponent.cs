﻿using AvaliacaoSawluz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoSawluz.Business
{
    public interface IMenuItemComponent
    {
        IEnumerable<MenuItem> ListMenuItems();
        MenuItem GetMenuItem(long id);
        void SaveMenuItem(MenuItem menuItem);
        void RemoveMenuItem(long id);
    }
}
