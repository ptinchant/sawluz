﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AvaliacaoSawluz.Data.Models;
using AvaliacaoSawluz.Data.Context;
using AvaliacaoSawluz.Business.Exceptions;
using System.Data.Entity.Migrations;
using System.Data.Entity;

namespace AvaliacaoSawluz.Business
{
    public class MenuItemComponent : IMenuItemComponent
    {
        public MenuItem GetMenuItem(long id)
        {
            RestaurantContext ctx = new RestaurantContext();
            return ctx.MenuItems.Include(i => i.Restaurant).FirstOrDefault(f => f.Id == id);
        }

        public IEnumerable<MenuItem> ListMenuItems()
        {
            RestaurantContext ctx = new RestaurantContext();
            return ctx.MenuItems.Include(i => i.Restaurant).ToList();
        }

        public void RemoveMenuItem(long id)
        {
            RestaurantContext ctx = new RestaurantContext();
            ctx.MenuItems.Remove(ctx.MenuItems.First(f => f.Id == id));
            ctx.SaveChanges();
        }

        public void SaveMenuItem(MenuItem menuItem)
        {
            RestaurantContext ctx = new RestaurantContext();

            if (ctx.MenuItems.Any(f => f.Name.ToLower() == menuItem.Name.ToLower() && f.RestaurantId == menuItem.RestaurantId && f.Id != menuItem.Id))
            {
                throw new AlreadyExistsException(menuItem.Name);
            }


            ctx.MenuItems.AddOrUpdate(f => f.Id, menuItem);
            ctx.SaveChanges();
        }
    }
}
