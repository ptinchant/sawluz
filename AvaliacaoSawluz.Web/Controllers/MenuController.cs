﻿using AvaliacaoSawluz.Business;
using AvaliacaoSawluz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvaliacaoSawluz.Web.Controllers
{
    public class MenuController : Controller
    {
        public ActionResult Index()
        {
            IMenuItemComponent bus = new MenuItemComponent();
            ViewBag.Name = string.Empty;
            return View(bus.ListMenuItems());
        }

        // GET: MenuItem/Create
        public ActionResult Create()
        {
            LoadRestaurantDropDown(null);
            return View();
        }

        // POST: MenuItem/Create
        [HttpPost]
        public ActionResult Create(MenuItem menuItem)
        {
            try
            {
                IMenuItemComponent bus = new MenuItemComponent();
                bus.SaveMenuItem(menuItem);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                LoadRestaurantDropDown(menuItem.RestaurantId);
                return View(menuItem);
            }
        }

        // GET: MenuItem/Edit/5
        public ActionResult Edit(long id)
        {
            IMenuItemComponent bus = new MenuItemComponent();
            MenuItem data = bus.GetMenuItem(id);
            LoadRestaurantDropDown(data.RestaurantId);
            return View(data);
        }

        // POST: MenuItem/Edit/5
        [HttpPost]
        public ActionResult Edit(MenuItem menuItem)
        {
            try
            {
                IMenuItemComponent bus = new MenuItemComponent();
                bus.SaveMenuItem(menuItem);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                LoadRestaurantDropDown(menuItem.RestaurantId);
                return View(menuItem);
            }
        }

        public JsonResult Delete(long id)
        {
            try
            {
                IMenuItemComponent bus = new MenuItemComponent();
                bus.RemoveMenuItem(id);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        private void LoadRestaurantDropDown(long? value)
        {
            List<SelectListItem> dropDownData = new List<SelectListItem>();
            dropDownData.Add(new SelectListItem { Text = "Selecione", Value = "0", Selected = !value.HasValue });
            IRestaurantComponent bus = new RestaurantComponent();
            IEnumerable<Restaurant> restaurants = bus.ListRestaurants();
            dropDownData.AddRange(restaurants.OrderBy(o => o.Name).Select(item => new SelectListItem { Text = item.Name, Value = item.Id.ToString(), Selected = value.HasValue && item.Id == value.Value }));
            ViewBag.Restaurants = dropDownData;
        }
    }
}