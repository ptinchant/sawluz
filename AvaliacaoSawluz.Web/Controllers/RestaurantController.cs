﻿using AvaliacaoSawluz.Business;
using AvaliacaoSawluz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AvaliacaoSawluz.Web.Controllers
{
    public class RestaurantController : Controller
    {
        // GET: Restaurant
        public ActionResult Index()
        {
            IRestaurantComponent bus = new RestaurantComponent();
            ViewBag.Name = string.Empty;
            return View(bus.ListRestaurants());
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            IRestaurantComponent bus = new RestaurantComponent();
            ViewBag.Name = name;
            IEnumerable<Restaurant> restaurants = bus.ListRestaurants().Where(f => f.Name.Contains(name));
            return View("Index", restaurants);
        }
        // GET: Restaurant/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Restaurant/Create
        [HttpPost]
        public ActionResult Create(Restaurant restaurant)
        {
            try
            {
                IRestaurantComponent bus = new RestaurantComponent();
                bus.SaveRestaurant(restaurant);
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(restaurant);
            }
        }

        // GET: Restaurant/Edit/5
        public ActionResult Edit(long id)
        {
            IRestaurantComponent bus = new RestaurantComponent();
            Restaurant data = bus.GetRestaurant(id);
            return View(data);
        }

        // POST: Restaurant/Edit/5
        [HttpPost]
        public ActionResult Edit( Restaurant restaurant)
        {
            try
            {
                IRestaurantComponent bus = new RestaurantComponent();
                bus.SaveRestaurant(restaurant);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(restaurant);
            }
        }

        public JsonResult Delete(long id)
        {
            try
            {
                IRestaurantComponent bus = new RestaurantComponent();
                bus.RemoveRestaurant(id);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
