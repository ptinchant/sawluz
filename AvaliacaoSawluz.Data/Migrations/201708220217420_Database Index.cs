namespace AvaliacaoSawluz.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseIndex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MenuItems", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MenuItems", "Name");
        }
    }
}
