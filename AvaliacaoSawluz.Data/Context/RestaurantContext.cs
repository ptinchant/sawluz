﻿using AvaliacaoSawluz.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoSawluz.Data.Context
{
    public class RestaurantContext: DbContext
    {
        public RestaurantContext() : base("Context")
        {
            base.Configuration.ProxyCreationEnabled = false;
        }
    public IDbSet<Restaurant> Restaurants { get; set; }
        public IDbSet<MenuItem> MenuItems { get; set; }
    }
}
