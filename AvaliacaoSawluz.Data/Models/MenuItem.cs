﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoSawluz.Data.Models
{
    public class MenuItem
    {
        [Key, DatabaseGenerated( DatabaseGeneratedOption.Identity)]
        public long Id{ get; set; }
        [DisplayName("Prato")]
        public string Name { get; set; }
        [DisplayName("Preço")]
        public decimal Price { get; set; }
        public long RestaurantId { get; set; }
        [ForeignKey("RestaurantId")]
        public Restaurant Restaurant { get; set; }
    }
}
