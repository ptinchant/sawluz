﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvaliacaoSawluz.Data.Models
{

    public class Restaurant
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [DisplayName("Restaurante")]
        public string Name { get; set; }

        public virtual IEnumerable<MenuItem> MenuItems { get; set; }
    }
}
